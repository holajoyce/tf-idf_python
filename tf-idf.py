#!/usr/bin/env python
# encoding: utf-8
#
# extended tf-idf example in Python from Tim Trueman's example
# by Joyce Chan provided under:
# 
# The MIT License
# Copyright (c) 2012 Joyce Chan 
# Copyright (c) 2009 Tim Trueman
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# http://www.opensource.org/licenses/mit-license.php

# obtained from https://github.com/timtrueman/tf-idf

# documents obtained from http://www.epicurious.com/recipes/food/views/Fish-Fillets-with-Tomatoes-Squash-and-Basil-395933?mbid=rss_epinr

from sets import Set
import math
import nltk
from nltk.tokenize.punkt import PunktWordTokenizer
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer  
from nltk.tokenize import WhitespaceTokenizer

from operator import itemgetter
import string
import re

def freq(word, document):
  # tokenized = PunktWordTokenizer().tokenize(document)
  # lst2 = [w.strip() for w in tokenized if w.strip() not in nltk.corpus.stopwords.words('english')]
  # # print lst2
  # return lst2.count(word)
  return document.count(word)

def wordCount(document):
  # return len(document.split(None))
  return len(document)

def numDocsContaining(word,documentList):
  count = 0
  for document in documentList:
    if freq(word,document) > 0:
      count += 1
  return count

def tf(word, document):
  return (freq(word,document) / float(wordCount(document)))

# do not want to divid by 0
def idf(word, documentList):
  word_in_doc = float(numDocsContaining(word,documentList))
  return math.log(len(documentList) / word_in_doc)

# def tfidf(word, document, documentList):
def tfidf(word, document, idfs):
  # document = document.decode('utf8')
  # return (tf(word,document) * idf(word,documentList))
  return tf(word,document) * float(idfs[word])

def removeRecipeStopWords(words):
  recipeStopWords=['prinkl','sprinkl','paper','crookneck','digit','season','june','may','july','august','september','october','kitchen','heavyduti','easil','easy','hard','mixtur','yield','appetit','inc','befor','slide','nutrit','activ','ani','quick','quickdish','among','crimp','four','summer','winter','fall','spring','autum','halloween','christmas','meal','lunch','dinner','snack','pan','pattypan','wine','ground','hour','atop','analysi','provid','cut','fiber','steam','escap','larg','before','temperatur','continu','right','dri','plu','use','seal','crim','mistur','ounc','drizzl','surfac','work','altern','calori','form','edg','amoung','toothpick','oven','made','portion','place','stand','chill','room','ahead','equal','yellow','garnish','g','c','f','x','fat','all','rights','reserv','open','cook','slid','singl','build','fire','charcoal','doubl','divid','assort','ga','poke','water','salt','pepper','add','around','foil','side','test','taste','make','amoung','one','two','three','arrang','half','halv','leav','medium','fresh','heat','cold','layer','rim','serv','tablespoon','teaspoon','per','bake','grill','stir','parchment','thi','cup','sheet','slice','packet','thin','minut','second','let','oil','pepper','white','black']
  return [w for w in words if w not in recipeStopWords]

def tokenize(text):  
    """ break up some abritrary text into tokens """  
    # get rid of all punctuation  
    rex = re.compile(r"[^A-Za-z\s]") 
    text = rex.sub('', text)  
  
    # create NLTK objects we'll need  
    stemmer = PorterStemmer()  
    tokenizer = WhitespaceTokenizer()  
  
    # break text up into words  
    tokens = tokenizer.tokenize(text)  
  
    # get the stems of the words  
    words = [stemmer.stem(token.lower()) for token in tokens]  

    # remove stop words
    noStopwords =  [w for w in words if w not in nltk.corpus.stopwords.words('english')]
  
    return noStopwords  

if __name__ == '__main__':
  documentList = []
  documentList_tokenized_lst = []

  # test set 1
  # documentList.append("""i got nottin""")
  # documentList.append("""i y i e i will always love u boots""")
  # documentList.append("""puss in the boots""")

  # test set 2
  # documentList.append("""Fish Fillets with Tomatoes, Squash, and Basil""")
  # documentList.append("""20 halved cherry tomatoes""")
  # documentList.append("""Bon Appétit | June 2012""")
  # documentList.append("""by The Bon Appétit Test Kitchen""")
  # documentList.append("""You can use any white flaky fish in this versatile—and quick—dish.""")
  # documentList.append("""Yield: Makes 4 servings """)
  # documentList.append("""Active Time: 20 minutes""")
  # documentList.append("""Active Time: 20 minutes""")
  # documentList.append("""2 cups very thinly sliced assorted summer squash (such as zucchini, yellow crookneck, and pattypan)""")
  # documentList.append("""1/4 cup thinly sliced shallots""")
  # documentList.append("""1/4 cup thinly sliced fresh basil, divided, plus 1/4 cup basil leaves""")
  # documentList.append("""4 tablespoons dry white wine""")
  # documentList.append("""4 tablespoons extra-virgin olive oil, divided""")
  # documentList.append("""Kosher salt, freshly ground pepper""")
  # documentList.append("""4 6-ounce skinless white flaky fish fillets (such as Atlantic cod or halibut)""")
  # documentList.append("""Place four 14x12" sheets of parchment paper, or heavy-duty foil if grilling, on a work surface. """)
  # documentList.append("""Divide squash among parchment sheets, arranging on one side of sheet in thin layer""")
  # documentList.append("""prinkle shallots and sliced basil over, dividing equally. Scatter tomato halves around squash. """)
  # documentList.append("""Drizzle each packet with 1 tablespoons wine and 1/2 tablespoon oil (add 1/2 tablespoon water to each if grilling). """)
  # documentList.append("""Season with salt and pepper. """)
  # documentList.append("""Place a fish fillet atop each portion""")
  # documentList.append("""Season with salt and pepper; drizzle 1/2 tablespoon oil over each""")
  # documentList.append("""Fold parchment over mixture and crimp edges tightly to form a sealed packet""")
  # documentList.append("""DO AHEAD: Can be made 4 hours ahead""")
  # documentList.append("""Chill""")
  # documentList.append("""Let stand at room temperature for 15 minutes before continuing""")
  # documentList.append("""Preheat oven to 400°F""")
  # documentList.append("""Place packets in a single layer on a large rimmed baking sheet""")
  # documentList.append("""Alternatively, build a medium fire in a charcoal grill, or heat a gas grill to medium-high""")
  # documentList.append("""Bake or grill fish until just cooked through (a toothpick poked through the parchment will slide through fish easily), about 10 minutes.""")
  # documentList.append("""Carefully cut open packets (steam will escape)""")
  # documentList.append("""Garnish with basil leaves""")
  # documentList.append("""Per serving: 300 calories, 15 g fat, 2 g fiber""")
  # documentList.append("""Nutritional analysis provided by Bon Appétit""")
  # documentList.append("""Epicurious.com © Condé Nast Digital, Inc. All rights reserved.""")

  # test set 3
  # documentList.append("""r r r r r r r r q q""")
  # documentList.append("""r r """)

  # test set 4
  documentList.append("""Fish Fillets with Tomatoes, Squash, and Basil Bon Appétit | June 2012 by The Bon Appétit Test Kitchen""")
  documentList.append("""You can use any white flaky fish in this versatile—and quick—dish.""")
  documentList.append("""Yield: Makes 4 servings  Active Time: 20 minutes Active Time: 20 minutes""")
  documentList.append("""20 halved cherry tomatoes 2 cups very thinly sliced assorted summer squash (such as zucchini, yellow crookneck, and pattypan) 1/4 cup thinly sliced shallots 1/4 cup thinly sliced fresh basil, divided, plus 1/4 cup basil leaves 4 tablespoons dry white wine tablespoons extra-virgin olive oil, divided Kosher salt, freshly ground pepper 4 6-ounce skinless white flaky fish fillets (such as Atlantic cod or halibut)""")
  documentList.append("""Place four 14x12" sheets of parchment paper, or heavy-duty foil if grilling, on a work surface.  Divide squash among parchment sheets, arranging on one side of sheet in thin layer prinkle shallots and sliced basil over, dividing equally. Scatter tomato halves around squash. Drizzle each packet with 1 tablespoons wine and 1/2 tablespoon oil (add 1/2 tablespoon water to each if grilling). Season with salt and pepper. Place a fish fillet atop each portion""")
  documentList.append("""Season with salt and pepper; drizzle 1/2 tablespoon oil over each Fold parchment over mixture and crimp edges tightly to form a sealed packet""")
  documentList.append("""DO AHEAD: Can be made 4 hours ahead Chill Let stand at room temperature for 15 minutes before continuing""")
  documentList.append("""Preheat oven to 400°F Place packets in a single layer on a large rimmed baking sheet Alternatively, build a medium fire in a charcoal grill, or heat a gas grill to medium-high Bake or grill fish until just cooked through (a toothpick poked through the parchment will slide through fish easily), about 10 minutes. Carefully cut open packets (steam will escape) Garnish with basil leaves""")
  documentList.append("""Per serving: 300 calories, 15 g fat, 2 g fiber Nutritional analysis provided by Bon Appétit""")
  documentList.append("""Epicurious.com © Condé Nast Digital, Inc. All rights reserved.""")

  words = {}
  idfs = {}
  tfs = {}
  wordcounts = {}
  documentNumber = 0
  numdocs = len(documentList)

  unique_words = Set([])

  # clean up documents
  for doc in documentList:
    newdoc = removeRecipeStopWords(tokenize(doc))
    unique_words = unique_words| Set(newdoc)
    documentList_tokenized_lst.append(newdoc)
  
  print unique_words
  print "\n"
  print documentList_tokenized_lst

  for doc in documentList[:]:
    for word in unique_words:
      if word in wordcounts:
        wordcounts[word] += freq(word,doc)
      else :
        wordcounts[word] = freq(word,doc)

      idfs[word] = idf(word,documentList_tokenized_lst)
      if word in tfs:
        tfs[word] = tfs[word]+math.pow(tf(word, doc),2)
      else:
        tfs[word] = tf(word, doc)

  for word in tfs.iterkeys():
    tfs[word] = math.sqrt(tfs[word])

  for word in tfs:
    words[word] = tfs[word] * idfs[word]


  print "tfs: "
  print tfs
  print "\nidfs:"  
  print idfs
  print "\nwords:"
  print words  
  print "\nwordcounts:"
  print wordcounts

  for item in sorted(wordcounts.items(), key=itemgetter(1), reverse=True):
    if item[1] > 1:
      print "%f <= %s" % (item[1], item[0])

  print "\nwords: "
  for item in sorted(words.items(), key=itemgetter(1), reverse=True):
    if item[1] > 0.00001:
      print "%f <= %s" % (item[1], item[0])


